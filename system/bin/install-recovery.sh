#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:27249998:b4c03030cab6a139d9e30fa8973f73a74cdd8377; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25484618:2c8d7c2f282abbe27bda9ac61d64b3d0bdc291be EMMC:/dev/block/bootdevice/by-name/recovery b4c03030cab6a139d9e30fa8973f73a74cdd8377 27249998 2c8d7c2f282abbe27bda9ac61d64b3d0bdc291be:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
