#!/system/bin/sh
#
# FTM (Factory test mode) script.
# Run this script under FTM mode.
#

source /data/ftm_status.sh
source /system/bin/scripts/ftm_common.sh

usage() {
	echo "usage: $0 [OPTIONS]...[Parameter_1]"
	echo "Performs FTM ( $0 ) TEST with the necessary parameters."
	echo "FTM Alive"
	echo "OPTIONS:"
	echo "-h    Show this message"
	echo "Parameters:"
	echo "Parameter_1 Alive"
}

FTM_Alive() {
	return_info
}

###
# Main body of script starts here
###

#Option parsing
while getopts ":h" OPTION ; do
	case $OPTION in
	h)  usage
		exit 0
		;;
	?)  echo "Illegal option: -$OPTARG"
		usage
		exit 1
		;;
	esac
done

#set watchdog for ftm
ftm_watchdog

#TIME Measure --- Start
START=$(time_start)

#convert parmater lower to upper case
parameter_1=$(case_converter $1)

#if you want to add debug message, use DEBUG ahead
DEBUG echo "$0 $1"

if [ "$parameter_1" = "ALIVE" ]; then
	FTM_Alive
else
	return_info "Illegal Parameter"
fi

#TIME Measure --- END
END=$(time_end)

#TIME Measure --- DIFF
DIFF=$(time_diff $END $START)
DEBUG echo "TIME DIFF $DIFF"
