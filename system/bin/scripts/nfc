#!/system/bin/sh
#
# FTM (Factory test mode) script.
# Run this script under FTM mode.
#

source /data/ftm_status
source /system/bin/scripts/ftm_common

usage() {
	echo "usage: $0 [OPTIONS]...[Parameter_1] [Parameter_2] [Parameter_3]"
	echo "Performs FTM ( $0 ) TEST with the necessary parameters."
	echo "NFC Ping/Reader/Card"
	echo "OPTIONS:"
	echo "-h    Show this message"
	echo "Parameters:"
	echo "Parameter_1 NFC_function"
	echo "Parameter_2 mode"
	echo "Parameter_3 WI/SWP"
}

FAILED="0"
SUCCESS="1"
NOTSUPPORTED="2"
TIMEOUT="3"
READ_DEVICE_FAIL="4"
READ_TAG_ERROR="5"
ALLOCATE_FAIL="6"
HW_RESET_FAILED="7"
UICC_NOT_CONNECT="8"
SWP_REGISTER_ERROR="9"
PING_SWP_TIMEOUT="10"
SWP_ACTIVATION_FAIL="11"
WRONG_SWP_STATE="12"
CREATE_FILE_FAILED="15"
READ_LENGTH_OUT_RANGE="18"
OPEN_DEVICE_FAIL="19"
WRITE_DEVICE_FAIL="20"
READ_TAG_TIMEOUT="21"
READ_VERSION_FAIL="22"

INVALID_OPT="13"
INIT_FAILED="14"
NO_DATA_TO_READ="16"
CLOSE_FAIL="17"

NFC_Ping() {
	busybox killall nfc_pn547_test > /dev/null 2>&1

	if [ "$_DEBUG" == "on" ]; then
		nfc_pn547_test -v -c NFC -p PING
	else
		nfc_pn547_test -c NFC -p PING
	fi

	ret=$?

	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$NOTSUPPORTED" ]; then
		return_info "Command not support"
	elif [ "$ret" = "$TIMEOUT" ]; then
		return_info "Command timeout"
	elif [ "$ret" = "$FAILED" ]; then
		return_info "Failed"
	elif [ "$ret" = "$OPEN_DEVICE_FAIL" ]; then
		return_info "Open device failed"
	elif [ "$ret" = "$WRITE_DEVICE_FAIL" ]; then
		return_info "Write device failed"
	elif [ "$ret" = "$READ_DEVICE_FAIL" ]; then
		return_info "read device failed"
	elif [ "$ret" = "$ALLOCATE_FAIL" ]; then
		return_info "Allocate failed"
	elif [ "$ret" = "$HW_RESET_FAILED" ]; then
		return_info "HW reset failed"
	elif [ "$ret" = "$READ_LENGTH_OUT_RANGE" ]; then
		return_info "Read length out of range"
	else
		return_info "Unknown error"
	fi
}

NFC_Reader_Mode() {
	busybox killall nfc_pn547_test > /dev/null 2>&1
	rm /data/nfc_id > /dev/null 2>&1

	if [ "$_DEBUG" == "on" ]; then
		nfc_pn547_test -v -c NFC -p READER
	else
		nfc_pn547_test -c NFC -p READER
	fi

	ret=$?

	busybox usleep 1000000

	echo "$(cat /data/nfc_id)"

	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$NOTSUPPORTED" ]; then
		return_info "Command not support"
	elif [ "$ret" = "$TIMEOUT" ]; then
		return_info "Command timeout"
	elif [ "$ret" = "$FAILED" ]; then
		return_info "Failed"
	elif [ "$ret" = "$OPEN_DEVICE_FAIL" ]; then
		return_info "Open device failed"
	elif [ "$ret" = "$WRITE_DEVICE_FAIL" ]; then
		return_info "Write device failed"
	elif [ "$ret" = "$READ_DEVICE_FAIL" ]; then
		return_info "read device failed"
	elif [ "$ret" = "$ALLOCATE_FAIL" ]; then
		return_info "Allocate failed"
	elif [ "$ret" = "$HW_RESET_FAILED" ]; then
		return_info "HW reset failed"
	elif [ "$ret" = "$READ_LENGTH_OUT_RANGE" ]; then
		return_info "Read length out of range"
	elif [ "$ret" = "$READ_TAG_ERROR" ]; then
		return_info "Read tag error"
	elif [ "$ret" = "$CREATE_FILE_FAILED" ]; then
		return_info "Create file failed"
	elif [ "$ret" = "$READ_TAG_TIMEOUT" ]; then
		return_info "Read tag timeout"
	else
		return_info "Unknown error"
	fi
}

NFC_Card_Mode() {
	busybox killall nfc_pn547_test > /dev/null 2>&1

	if [ "$1" = "SWP"  ]; then
		if [ "$_DEBUG" == "on" ]; then
			nfc_pn547_test -v -c NFC -p SWP
		else
			nfc_pn547_test -c NFC -p SWP
		fi
	elif  [ "$1" = "WI"  ]; then
		return_info "NOT SUPPORT"
	else
		return_info "NOT SUPPORT"
	fi

	ret=$?

	if [ "$ret" = "$SUCCESS" ]; then
		return_info
		if [ "$_DEBUG" == "on" ]; then
			nfc_pn547_test -v -c NFC -p POLL &
		else
			nfc_pn547_test -c NFC -p POLL &
		fi
	elif [ "$ret" = "$NOTSUPPORTED" ]; then
		return_info "Command not support"
	elif [ "$ret" = "$TIMEOUT" ]; then
		return_info "Command timeout"
	elif [ "$ret" = "$FAILED" ]; then
		return_info "Failed"
	elif [ "$ret" = "$OPEN_DEVICE_FAIL" ]; then
		return_info "Open device failed"
	elif [ "$ret" = "$WRITE_DEVICE_FAIL" ]; then
		return_info "Write device failed"
	elif [ "$ret" = "$READ_DEVICE_FAIL" ]; then
		return_info "read device failed"
	elif [ "$ret" = "$ALLOCATE_FAIL" ]; then
		return_info "Allocate failed"
	elif [ "$ret" = "$HW_RESET_FAILED" ]; then
		return_info "HW reset failed"
	elif [ "$ret" = "$READ_LENGTH_OUT_RANGE" ]; then
		return_info "Read length out of range"
	elif [ "$ret" = "$UICC_NOT_CONNECT" ]; then
		return_info "UICC is not connected"
	else
		return_info "Unknown error"
	fi
}

NFC_SWP_SelfTest() {
	busybox killall nfc_pn547_test > /dev/null 2>&1

	if [ "$_DEBUG" == "on" ]; then
		nfc_pn547_test -v -c NFC -p SWP_SELFTEST
	else
		nfc_pn547_test -c NFC -p SWP_SELFTEST
	fi

	ret=$?

	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$NOTSUPPORTED" ]; then
		return_info "Command not support"
	elif [ "$ret" = "$TIMEOUT" ]; then
		return_info "Command timeout"
	elif [ "$ret" = "$FAILED" ]; then
		return_info "Failed"
	elif [ "$ret" = "$OPEN_DEVICE_FAIL" ]; then
		return_info "Open device failed"
	elif [ "$ret" = "$WRITE_DEVICE_FAIL" ]; then
		return_info "Write device failed"
	elif [ "$ret" = "$READ_DEVICE_FAIL" ]; then
		return_info "read device failed"
	elif [ "$ret" = "$ALLOCATE_FAIL" ]; then
		return_info "Allocate failed"
	elif [ "$ret" = "$HW_RESET_FAILED" ]; then
		return_info "HW reset failed"
	elif [ "$ret" = "$READ_LENGTH_OUT_RANGE" ]; then
		return_info "Read length out of range"
	elif [ "$ret" = "$PING_SWP_TIMEOUT" ]; then
		return_info "SWP ping timeout"
	elif [ "$ret" = "$SWP_ACTIVATION_FAIL" ]; then
		return_info "SWP activation failed"
	elif [ "$ret" = "$WRONG_SWP_STATE" ]; then
		return_info "Wrong SWP state"
	else
		return_info "Unknown error"
	fi
}

NFC_Get_SW_FW_Version() {
	busybox killall nfc_pn547_test > /dev/null 2>&1
	rm /data/nfc_sw_version > /dev/null 2>&1
	rm /data/nfc_hw_version > /dev/null 2>&1

	if [ "$_DEBUG" == "on" ]; then
		nfc_pn547_test -v -c NFC -p GET_VERSION
	else
		nfc_pn547_test -c NFC -p GET_VERSION
	fi

	ret=$?

	busybox usleep 1000000

	echo "$(cat /data/nfc_sw_version)"

	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$NOTSUPPORTED" ]; then
		return_info "Command not support"
	elif [ "$ret" = "$TIMEOUT" ]; then
		return_info "Command timeout"
	elif [ "$ret" = "$FAILED" ]; then
		return_info "Failed"
	elif [ "$ret" = "$OPEN_DEVICE_FAIL" ]; then
		return_info "Open device failed"
	elif [ "$ret" = "$WRITE_DEVICE_FAIL" ]; then
		return_info "Write device failed"
	elif [ "$ret" = "$READ_DEVICE_FAIL" ]; then
		return_info "read device failed"
	elif [ "$ret" = "$ALLOCATE_FAIL" ]; then
		return_info "Allocate failed"
	elif [ "$ret" = "$HW_RESET_FAILED" ]; then
		return_info "HW reset failed"
	elif [ "$ret" = "$READ_LENGTH_OUT_RANGE" ]; then
		return_info "Read length out of range"
	elif [ "$ret" = "$CREATE_FILE_FAILED" ]; then
		return_info "Create file failed"
	elif [ "$ret" = "$READ_VERSION_FAIL" ]; then
		return_info "Read firmware version fail"
	else
		return_info "Unknown error"
	fi
}

###
# Main body of script starts here
###

#Option parsing
while getopts ":h" OPTION ; do
	case $OPTION in
	h)  usage
		exit 0
		;;
	?)  echo "Illegal option: -$OPTARG"
		usage
		exit 1
		;;
	esac
done

#set watchdog for ftm
ftm_watchdog

#TIME Measure --- Start
START=$(time_start)

#convert parmater lower to upper case
parameter_1=$(case_converter $1)
parameter_3=$(case_converter $3)

device=$(getprop ro.boot.device)
###bandinfo=$(cat /proc/bandinfo)
###DEBUG echo "device = $device, bandinfo = $bandinfo"

#if you want to add debug message, use DEBUG ahead
DEBUG echo "$0 $1 $2 $3"

###if [ "$device" = "vn2" ] && [ "$bandinfo" = "G_1800_1900^W_1_8^L_3_40" ]; then
###	return_info "NOT SUPPORT"
if [ "$device" = "fag" ]; then
	return_info "NOT SUPPORT"
elif [ "$parameter_1" = "PING"  ]; then
	NFC_Ping
elif [ "$parameter_1" = "READER" ]; then
	NFC_Reader_Mode
elif [ "$parameter_1" = "CARD" ]; then
	NFC_Card_Mode $parameter_3
elif [ "$parameter_1" = "SWP" ]; then
	return_info "NOT SUPPORT"
elif [ "$parameter_1" = "GETVERSION" ]; then
	NFC_Get_SW_FW_Version
elif [ "$parameter_1" = "RW" ]; then
	return_info "NOT SUPPORT"
elif [ "$parameter_1" = "SIM" ]; then
	return_info "NOT SUPPORT"
else
	return_info "Illegal Parameter"
fi

#TIME Measure --- END
END=$(time_end)

#TIME Measure --- DIFF
DIFF=$(time_diff $END $START)
DEBUG echo "TIME DIFF $DIFF"
