#!/system/bin/sh
#
# FTM (Factory test mode) script.
# Run this script under FTM mode.
#

source /data/ftm_status
source /system/bin/scripts/ftm_common

usage() {
	echo "usage: $0 [OPTIONS]...[Parameter_1]"
	echo "Performs FTM ( $0 ) TEST with the necessary parameters."
	echo "SIM GetStatus/Count"
	echo "OPTIONS:"
	echo "-h    Show this message"
	echo "Parameters:"
	echo "Parameter_1 SUM_function"
}

SUCCESS="0"
INVALID_OPT="1"
ERR_OPENFAILED="3"
ERR_NULL_LIBRIL="4"
ERR_QMI_CLIENT_INIT="5"
ERR_DMS_REQUEST="6"
ERR_QMI_RELEASE="7"
ERR_QMI_MODE="8"
ERR_QMI_PING="9"
ERR_QMI_RES="0xb"
ERR_SIM_COUNT_REQUEST="0xd"
ERR_COUNT_ZERO="0xe"


SIM_Result() {
	if [ "$ret" = "$SUCCESS" ]; then
		return_info
	elif [ "$ret" = "$ERR_NULL_LIBRIL" ]; then
		return_info "libril_qc_qmi is NULL"
	elif [ "$ret" = "$ERR_QMI_CLIENT_INIT" ]; then
		return_info "ftm_qmi_client_init fail"
	elif [ "$ret" = "$ERR_SIM_COUNT_REQUEST" ]; then
		return_info "ftm_sim_count_request fail"
	elif [ "$ret" = "$ERR_QMI_RELEASE" ]; then
		return_info "ftm_qmi_client_release fail"
	elif [ "$ret" = "$ERR_COUNT_ZERO" ]; then
		return_info "ERR_COUNT_ZERO"
	else
		return_info "Unknow message"
	fi
}

SIM_GetStatus() {
	qmi_ping_test 7 > /data/sim_getstatus_result

	SIM_GETSTATUS_RESULT=$(cat /data/sim_getstatus_result | grep "fih_get_sim_status_result" | busybox cut -d ">" -f 2)

	if [ "$SIM_GETSTATUS_RESULT" == "1" ]; then
		echo "Status=insert"
		return_info
	else
		echo "Status=not insert"
		return_info
	fi
}

SIM_Count() {
	if [ "$_DEBUG" == "on" ]; then
		modem_fih_test -v -c simcount
	else
		modem_fih_test -c simcount
	fi

	ret=$?

	SIM_Result $ret
}

###
# Main body of script starts here
###

#Option parsing
while getopts ":h" OPTION ; do
	case $OPTION in
	h)  usage
		exit 0
		;;
	?)  echo "Illegal option: -$OPTARG"
		usage
		exit 1
		;;
	esac
done

#set watchdog for ftm
ftm_watchdog

#TIME Measure --- Start
START=$(time_start)

#convert parmater lower to upper case
parameter_1=$(case_converter $1)

#if you want to add debug message, use DEBUG ahead
DEBUG echo "$0 $1"

if [ "$parameter_1" = "GETSTATUS"  ]; then
	SIM_GetStatus
elif [ "$parameter_1" = "COUNT" ]; then
	SIM_Count
elif [ "$parameter_1" = "EMULATE" ]; then
	return_info "NOT SUPPORT"
else
	return_info "Illegal Parameter"
fi

#TIME Measure --- END
END=$(time_end)

#TIME Measure --- DIFF
DIFF=$(time_diff $END $START)
DEBUG echo "TIME DIFF $DIFF"
